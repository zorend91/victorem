jQuery(document).ready(function ($) {
  $('.product-section-info-tab-title').click(function (e) {
    e.preventDefault();
    $(this).parent().toggleClass('active');
  });
  var mb_h = $('.product-section-middle-banner').outerHeight();
  $('.product-section-middle-banner .overlay').css('border-top-width', mb_h + 'px');

  function get_fbt_price() {
    var sale = 0;
    var was = 0;
    var save;
    if ($('.fbt-checkbox.active').length >= 1) {
      $('.fbt-price-summary .was').show();
      // $('.fbt-price-summary .save').show();
      $('.fbt-add a').removeAttr('disabled');
      $('.fbt-checkbox.active').each(function () {
        sale = sale + $(this).data('price');
        was = was + $(this).data('sale');
      });
      var save_money = was - sale;
      var save_percent = Math.round((save_money * 100) / was);
      save_money = save_money / 100;
      was = was / 100;
      sale = sale / 100;
      save = 'Save ' + save_percent + '% ($' + save_money + ')';
      $('.fbt-price-summary .sale').text('$' + sale);
      $('.fbt-price-summary .was').text('$' + was);
      // $('.fbt-price-summary .save').text(save);
    } else {
      $('.fbt-add a').attr('disabled', 'disabled');
      $('.fbt-price-summary .sale').text('Please Choose Products.');
      $('.fbt-price-summary .was').hide();
      $('.fbt-price-summary .save').hide();
    }
  }

  if (localStorage.getItem('browsing_history') !== null) {
    var json_str = localStorage.getItem('browsing_history');
    var browsing_history = JSON.parse(json_str);
    for (let i = 0; i < 4; i++) {
      console.log(browsing_history[i]);
      if (browsing_history[i] != undefined) {
        $('.browsing-list').append("<div class='item'>\n" +
          "    <a class='img' href='" + browsing_history[i].url + "'><img src='" + browsing_history[i].image + "'></a>" +
          "                <div class='title'><a href='" + browsing_history[i].url + "'>" + browsing_history[i].title + "</a></div>\n" +
          "            </div>");
      }
    }
  }

  $('.similar-item-add a').click(function (e) {
    e.preventDefault();
    var acsCart = [];
    var variant = {};
    variant.id = $(this).data('variant');
    variant.qty = 1;

    //data-pre-order-attr
    acsCart.push(variant);
    if (acsCart.length > 0) {
      // MGUtil.data = acsCart;
      // MGUtil.total = MGUtil.data.length;
      // MGUtil.action = 'add';
      // MGUtil.recursive();
      jQuery.post('/cart/add.js', { items: acsCart },
        () => {
          $(this).attr('disabled', 'disabled');
          $(this).text('added');
          setTimeout(() => {
            $(this).removeAttr('disabled');
            $(this).text('Add to Cart');
          }, 1000);
        }
        , "json")
        .fail(e => alert(e.responseJSON.description));
    }
    // $(this).removeAttr('disabled');
    // $(this).text('Add to Cart');
  });

  $('.faq-item-question').click(function (e) {
    e.preventDefault();
    if ($(this).attr('data-status') == 'opened') {
      $(this).attr('data-status', 'closed');
    } else {
      $(this).attr('data-status', 'opened');
    }
  });

  $('.fbt-add a').click(function (e) {
    e.preventDefault();
    if ($('.fbt-item.active').length >= 1) {
      var acsCart = [];
      $('.fbt-item.active').each(function () {
        var variant = {
          id: $(this).data('variant'),
          qty: 1
        };

        //data-pre-order-attr
        acsCart.push(variant);
      });
      if (acsCart.length > 0) {
        // MGUtil.data = acsCart;
        // MGUtil.total = MGUtil.data.length;
        // MGUtil.action = 'add';
        // MGUtil.recursive();
        $.post('/cart/add.js', {
          items: acsCart
        });
      }
    } else {
      return false;
    }
  });
  get_fbt_price();

  $('.fbt-checkbox').click(function (e) {
    e.preventDefault();
    var variant_id = $(this).data('variant');
    $('.fbt-checkbox[data-variant="' + variant_id + '"]').toggleClass('active');
    get_fbt_price();
  });
});